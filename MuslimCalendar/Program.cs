﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace MuslimCalendar
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ApplicationExit += Application_ApplicationExit;
                Application.Run(new Form1());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// Return checksum of a file.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private static string GetChecksum(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                return BitConverter.ToString(checksum).Replace("-", String.Empty);
            }
        }
        private static void Application_ApplicationExit(object sender, EventArgs e)
        {
            // Download latest muslim calendar if any
            System.Diagnostics.Debug.WriteLine("Download latest muslim calendar...");
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                // ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
                using (var client = new WebClient())
                {
                    client.DownloadFile("https://gitlab.com/hlgranite/muslimcalendar/raw/master/muslimcal.xml", "muslimcal2.xml");
                }

                // replace old file to new file
                if (!File.Exists(@"muslimcal.xml"))
                {
                    File.Move("muslimcal2.xml", "muslimcal.xml");
                }
                else
                {
                    string checksum1 = GetChecksum(@"muslimcal.xml");
                    string checksum2 = GetChecksum(@"muslimcal2.xml");
                    if (checksum1.Equals(checksum2))
                    {
                        File.Delete("muslimcal2.xml");
                    }
                    else
                    {
                        File.Delete("muslimcal.xml");
                        File.Move("muslimcal2.xml", "muslimcal.xml");
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }
    }
}