# MuslimCalendar
Muslim calendar based on Malaysia Jakim standard.

## Download
[![build status](https://gitlab.com/hlgranite/muslimcalendar/badges/master/build.svg)](https://gitlab.com/hlgranite/muslimcalendar/pipelines)

![screen](screen.png)

## Bulan Melayu
1. Muharram محرّم
2. Safar صفر
3. Rabiulawal ربيع الاول
4. Rabiulakhir ربيع الاخير
5. Jamadilawal جمادالاول
6. Jamadilakhir جمادالاخير
7. Rejab رجب
8. Syaaban شعبان
9. Ramadhan رمضان
10. Syawal شوال
11. Zulkaedah ذوالقعده
12. Zulhijjah ذوالحجه
